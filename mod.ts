import { Arango } from "https://deno.land/x/darango@0.1.6/mod.ts";
import type {
  Collection,
  EdgeCollection,
} from "https://deno.land/x/darango@0.1.6/mod.ts";
import type {
  ArangoAuthentication,
  Author,
  Domains,
  Favorite,
  FullStory,
  LeadDump,
  MicroUID,
  MiniUID,
  Review,
  ReviewGroup,
  ReviewPointer,
  Story,
  StoryDump,
  StoryID,
  User,
  UserDump,
  UserID,
} from "./types.ts";
export {
  ArangoAuthentication,
  Author,
  Domains,
  Favorite,
  FullStory,
  LeadDump,
  MicroUID,
  MiniUID,
  Review,
  ReviewGroup,
  ReviewPointer,
  Story,
  StoryDump,
  StoryID,
  User,
  UserDump,
  UserID,
};

export class RecEngineStore {
  private DB: Arango;
  private stories: Collection<Story> | undefined;
  private users: Collection<User> | undefined;
  private authors: EdgeCollection<Story, User> | undefined;
  private authorsRaw: Collection<Author> | undefined;
  private favorites: EdgeCollection<Story, User> | undefined;
  private favoritesRaw: Collection<Favorite> | undefined;
  private reviews: Collection<ReviewPointer> | undefined;
  constructor(db: Arango) {
    this.DB = db;
  }

  static async connect(
    host: string,
    database: string,
    authentication: ArangoAuthentication,
    port = 8529,
  ) {
    if (authentication.username && authentication.password) {
      return await Arango.basicAuth({
        uri: `http://${host}:${port}/_db/${database}`,
        username: authentication.username,
        password: authentication.password,
      });
    }
    /*if (authentication.jwt) {
      return await Arango.jwtAuth({
        uri: `http://${host}:${port}/_db/${database}`,
        jwt: authentication.jwt,
      });
    }*/
    throw new Error("Invalid Authentication Method");
  }
  public static isDomain(domain: string) {
    switch (domain) {
      case "ffnet":
        return true;
    }
    throw new Error("Invalid Domain");
  }

  public static isStoryID(key: StoryID) {
    if (!key.startsWith("stories/")) {
      throw new Error("Invalid Story Key");
    }
  }

  public static isUserID(key: UserID) {
    if (!key.startsWith("users/")) {
      throw new Error("Invalid User Key");
    }
  }

  public static areIDs(userK: UserID, storyK: StoryID) {
    RecEngineStore.isStoryID(storyK);
    RecEngineStore.isUserID(userK);
  }

  private static ca<T>(
    col: Collection<T> | undefined,
  ): Collection<T> {
    if (!col) {
      throw new Error("preConnect hasn't been run.");
    }
    return col as Collection<T>;
  }

  private static eca<T, U>(
    col: EdgeCollection<T, U> | undefined,
  ): EdgeCollection<T, U> {
    if (!col) {
      throw new Error("preConnect hasn't been run.");
    }
    return col as EdgeCollection<T, U>;
  }

  public static async FastSetup(
    host: string,
    database: string,
    authentication: ArangoAuthentication,
    port = 8529,
  ) {
    const store = new RecEngineStore(
      await RecEngineStore.connect(
        host,
        database,
        authentication,
        port,
      ),
    );
    await store.preConnect();
    return store;
  }

  async preConnect() {
    this.stories = await this.DB.collection<Story>("stories");
    this.users = await this.DB.collection<User>("users");
    this.authors = await this.DB.edgeCollection<Story, User>("authors");
    this.authorsRaw = await this.DB.collection<Author>("authors");
    this.favorites = await this.DB.edgeCollection<Story, User>("favorites");
    this.favoritesRaw = await this.DB.collection<Favorite>("favorites");
    this.reviews = await this.DB.collection<ReviewPointer>("reviews");
  }

  public async query(aql: string) {
    return await this.DB.query(aql);
  }

  // Story Stuff
  async storyById(domain: Domains | string, id: number) {
    RecEngineStore.isDomain(domain);
    const stories = RecEngineStore.ca<Story>(this.stories);
    return await stories.findOne({ domain: domain, id: id }) || null;
  }
  async addStory(story: Story) {
    const stories = RecEngineStore.ca<Story>(this.stories);
    RecEngineStore.isDomain(story.domain);
    const found = await this.storyById(story.domain, story.id);
    if (found?.id == story.id && found?.domain == story.domain) {
      //found._key = story._key
      //found.id = story.id
      //found.domain = story.domain
      found.name = story.name;
      found.wordcount = story.wordcount;
      if (story.leadcount) {
        found.leadcount = story.leadcount;
      }
      if (story.lastScrape) {
        found.lastScrape = story.lastScrape;
      }
      found.datesubmit = story.datesubmit;
      found.dateupdate = story.dateupdate;
      found.chapters = story.chapters;
      found.statusid = story.statusid;
      found.blurb = story.blurb;
      found.fandoms = story.fandoms;
      found.tags = story.tags;
      await found.update();
      return found;
    } else {
      return await stories.create(story);
    }
  }

  // User Stuff
  async userByName(domain: Domains, name: string) {
    RecEngineStore.isDomain(domain);
    const users = RecEngineStore.ca<User>(this.users);
    return await users.findOne({ domain, name }) || null;
  }
  async userById(domain: Domains, id: number) {
    RecEngineStore.isDomain(domain);
    const users = RecEngineStore.ca<User>(this.users);
    return await users.findOne({ domain, id }) || null;
  }
  async addUser(user: User) {
    RecEngineStore.isDomain(user.domain);
    const users = RecEngineStore.ca<User>(this.users);
    const found = await this.userById(user.domain, user.id);
    if (found == null) {
      return await users.create(user);
    } else {
      //found._key = user._key;
      //found.id = user.id;
      found.name = user.name;
      if (user.lastScrape) {
        found.lastScrape = user.lastScrape;
      }
      //found.domain = user.domain;
      await found.update();
      return found;
    }
  }

  // Review Stuff
  async storyReviews(storyK: StoryID) {
    const reviews = RecEngineStore.ca<ReviewPointer>(this.reviews);
    RecEngineStore.isStoryID(storyK);
    return await reviews.find({ _from: storyK });
  }
  async findReview(storyK: StoryID, userK: UserID) {
    const reviews = RecEngineStore.ca<ReviewPointer>(this.reviews);
    RecEngineStore.areIDs(userK, storyK);
    return await reviews.findOne({
      _from: storyK,
      _to: userK,
    });
  }
  async addReview(review: ReviewPointer) {
    const reviews = RecEngineStore.ca<ReviewPointer>(this.reviews);
    RecEngineStore.areIDs(review._to, review._from);
    const found = await this.findReview(review._from, review._to);
    if (!found) {
      return await reviews.create(review);
    } else {
      //found._key = review._key;
      //found._from = review._from;
      //found._to = review._to;
      found.reviews = { ...found.reviews, ...review.reviews };
      await found.update();
      return found;
    }
  }

  // Fave Stuff
  async workFaved(storyK: StoryID) {
    const favorites = RecEngineStore.eca<Story, User>(this.favorites);
    RecEngineStore.isStoryID(storyK);
    return await favorites.in(storyK);
  }
  async userFaves(userK: UserID) {
    const favorites = RecEngineStore.eca<Story, User>(this.favorites);
    RecEngineStore.isUserID(userK);
    return await favorites.out(userK);
  }
  async findFave(userK: UserID, storyK: StoryID) {
    const favorites = RecEngineStore.ca<Favorite>(this.favoritesRaw);
    RecEngineStore.areIDs(userK, storyK);
    return await favorites.findOne({
      _from: userK,
      _to: storyK,
    });
  }

  async addFave(fave: Favorite) {
    const favorites = RecEngineStore.ca<Favorite>(this.favoritesRaw);
    RecEngineStore.areIDs(fave._from, fave._to);
    const found = await this.findFave(fave._from, fave._to);
    if (!found) {
      return await favorites.create(fave);
    } else {
      /* No reason to update given nothing to update.
      //found._key = fave._key;
      //found._from = fave._from;
      //found._to = fave._to;
      await found.update();*/
      return found;
    }
  }

  // Author Stuff
  async authorsWork(userK: UserID) {
    const authors = RecEngineStore.eca<Story, User>(this.authors);
    RecEngineStore.isUserID(userK);
    return await authors.out(userK);
  }
  async workAuthors(storyK: StoryID) {
    const authors = RecEngineStore.eca<Story, User>(this.authors);
    RecEngineStore.isStoryID(storyK);
    return await authors.in(storyK);
  }
  async findAuthor(userK: UserID, storyK: StoryID) {
    const authors = RecEngineStore.ca<Favorite>(this.authorsRaw);
    RecEngineStore.areIDs(userK, storyK);
    return await authors.findOne({
      _from: userK,
      _to: storyK,
    });
  }
  async addAuthor(author: Author) {
    const authors = RecEngineStore.ca<Favorite>(this.authorsRaw);
    RecEngineStore.areIDs(author._from, author._to);
    const found = await this.findAuthor(author._from, author._to);
    if (!found) {
      return await authors.create(author);
    } else {
      /* No reason to update given nothing to update.
      //found._key = author._key;
      //found._from = author._from;
      //found._to = author._to;
      await found.update();*/
      return found;
    }
  }

  // Dump Stuff
  //TODO: Fix this so user names can be used since AO3 doesn't have a reverse function?
  async dumpUser(domain: Domains, userID: number) {
    RecEngineStore.isDomain(domain);
    const query = `WITH authors, favorites
FOR u IN users
FILTER u.domain == "${domain}"
FILTER u.id == ${userID}
LET authored = (
    FOR story IN OUTBOUND u authors
    RETURN {
        domain: story.domain,
        id: story.id,
        name: story.name
    }
)
LET faved = (
    FOR story IN OUTBOUND u favorites
    RETURN {
        domain: story.domain,
        id: story.id,
        name: story.name
    }
)
RETURN {
domain: u.domain,
id: u.id,
name: u.name,
lastScrape: u.lastScrape,
authored,
favedBooks: faved,
}`;
    const results = (await (await this.query(query)).collect());
    if (results.length == 0) {
      return null;
    }
    return results[0] as UserDump;
  }
  async dumpStory(domain: Domains, storyID: number) {
    RecEngineStore.isDomain(domain);
    const query = `WITH authors, favorites
FOR s IN stories
FILTER s.domain == "${domain}"
FILTER s.id == ${storyID}
LET authored = (
    FOR user IN INBOUND s authors
    RETURN {
        domain: user.domain,
        id: user.id,
        name: user.name
    }
)
LET faved = (
    FOR user IN INBOUND s favorites
    RETURN {
        domain: user.domain,
        id: user.id,
        name: user.name
    }
)
RETURN {
id: s.id,
domain: s.domain,
details: s,
authors: authored,
faved
}`;
    const results = (await (await this.query(query)).collect());
    if (!results[0]) {
      return null;
    }
    return results[0] as StoryDump;
  }
  async dumpLeads(domain: Domains, storyID: number) {
    RecEngineStore.isDomain(domain);
    const query = `WITH reviews
FOR s IN stories
FILTER s.domain == "${domain}"
FILTER s.id == ${storyID}
LET reviewed = (
    FOR user IN OUTBOUND s reviews
    RETURN {
        domain: user.domain,
        id: user.id,
        name: user.name
    }
)
RETURN {
domain: s.domain,
id: s.id,
leadcount: s.leadcount,
reviewed,
}`;
    const results = (await (await this.query(query)).collect());
    if (results.length == 0) {
      return null;
    }
    return results[0] as LeadDump;
  }
}
