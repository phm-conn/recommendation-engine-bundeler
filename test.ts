import * as test from "https://deno.land/std@0.133.0/testing/asserts.ts";

import { RecEngineStore } from "./mod.ts";
import type {
  Review,
  ReviewGroup,
  ReviewPointer,
  Story,
  StoryID,
  User,
  UserID,
} from "./types.ts";

Deno.test("Invalid Auth Method", async () => {
  await test.assertRejects(async () => {
    await RecEngineStore.connect(
      "localhost",
      "testing",
      { jwt: "" },
    );
  });
});

Deno.test("Invalid Keys", async (t) => {
  await t.step("Invalid story key", () => {
    test.assertThrows(
      () => {
        RecEngineStore.areIDs(`555`, `stories/555`);
      },
      undefined,
      undefined,
      "Invalid User Key",
    );
  });
  await t.step("Invalid User Key", () => {
    test.assertThrows(
      () => {
        RecEngineStore.areIDs(`users/555`, `555`);
      },
      undefined,
      "Invalid Story Key",
    );
  });
  await t.step("Both Fine", () => {
    RecEngineStore.areIDs(`users/555`, `stories/555`);
  });
});

// TODO: This test can probably be refactored later.
Deno.test("Reject without preconnect", async (t) => {
  const store = new RecEngineStore(
    await RecEngineStore.connect(
      "localhost",
      "testing",
      {
        username: "testing",
        password: "test",
      },
    ),
  );
  await t.step("storyById", async () => {
    await test.assertRejects(
      async () => {
        await store.storyById("ffnet", 0);
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("userById", async () => {
    await test.assertRejects(
      async () => {
        await store.userById("ffnet", 0);
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("userByName", async () => {
    await test.assertRejects(
      async () => {
        await store.userByName("ffnet", "hI");
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("storyReviews", async () => {
    await test.assertRejects(
      async () => {
        await store.storyReviews(`stories/555`);
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("favedByUsers", async () => {
    await test.assertRejects(
      async () => {
        await Promise.reject(new Error("TODO"));
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("userFaves", async () => {
    await test.assertRejects(
      async () => {
        await store.userFaves(`users/555`);
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("authorsWork", async () => {
    await test.assertRejects(
      async () => {
        await store.authorsWork("users/961");
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
  await t.step("workAuthors", async () => {
    await test.assertRejects(
      async () => {
        await store.workAuthors("stories/1500");
      },
      undefined,
      undefined,
      "preConnect hasn't been run.",
    );
  });
});

Deno.test({
  name: "Wipe Data",
  ignore: true,
  async fn(t) {
    let store: RecEngineStore | null = null;
    await t.step("Connect to local test db", async () => {
      store = await RecEngineStore.FastSetup(
        "localhost",
        "testing",
        {
          username: "testing",
          password: "test",
        },
      );
    });

    await t.step("Clear All Data", async () => {
      if (!store) {
        test.unreachable();
      }
      const collections = [
        "stories",
        "users",
        "authors",
        "favorites",
        "reviews",
      ];
      for (const collection of collections) {
        await (await store.query(
          `FOR r IN ${collection}
REMOVE r IN ${collection}`,
        )).collect();
      }
    });
  },
});

Deno.test({
  name: "DB Testing",
  async fn(t) {
    let store: RecEngineStore | null = null;
    let storyID: StoryID | null = null;
    let userID: UserID | null = null;

    const story1: Story = {
      id: 14064084,
      domain: "ffnet",
      name: "Target Fic for API Testing",
      wordcount: 141,
      leadcount: 1,
      datesubmit: 1649314595,
      dateupdate: 1649314595,
      lastScrape: 1649314595 + 60,
      chapters: 1,
      statusid: 1,
      blurb:
        "This is a target for testing API's. It is meant to act as a safe testing target for a API to ensure it is working.",
      fandoms: [
        "Parodies and Spoofs",
      ],
      tags: [
        "OC",
      ],
    };
    const story2: Story = {
      id: 11975691,
      domain: "ffnet",
      name: "Well that's disappointing",
      wordcount: 674,
      leadcount: 1,
      datesubmit: 1464775151,
      dateupdate: 1464775151,
      lastScrape: 1464775151 + 60,
      chapters: 1,
      statusid: 1,
      blurb:
        "Two men discus the results of a program and debate on what to do. PLEASE COMMENT ON Archive Of Our Own! (This will have a unfailing ending so other's can make their own continuations, I will also make my own continuation of course.) THIS IS COMPLETELY AU.",
      fandoms: [
        "Person of Interest",
      ],
      tags: [
        "H. Finch",
        "Nathan I.",
        "Machine",
      ],
    };
    const story3: Story = {
      ...story1,
      dateupdate: 1649314595 + 10000,
      lastScrape: 1649314595 + 10000 + 60,
      leadcount: 2,
      tags: [
        ...story1.tags,
        "Updated",
      ],
    };
    const user1: User = {
      id: 15379546,
      name: "phantom-connections",
      domain: "ffnet",
      lastScrape: 1649314595,
    };
    const user2: User = {
      id: 4463712,
      name: "wwjdtd",
      domain: "ffnet",
      lastScrape: 1649314595,
    };
    const user3: User = {
      ...user1,
      name: "phantom-connections-renamed",
      lastScrape: 1649314595 + 10,
    };
    const review1: Review = {
      "time": 1649314866,
      "review": "Test Review\n235711",
    };
    const review2: Review = {
      "time": 555,
      "review": "Test Review\n314",
    };

    test.assertNotEquals(story1.dateupdate, story3.dateupdate);

    await t.step("Connect to local test db", async () => {
      store = await RecEngineStore.FastSetup(
        "localhost",
        "testing",
        {
          username: "testing",
          password: "test",
        },
      );
    });

    await t.step("Clear All Data", async () => {
      if (!store) {
        test.unreachable();
      }
      const collections = [
        "stories",
        "users",
        "authors",
        "favorites",
        "reviews",
      ];
      for (const collection of collections) {
        await (await store.query(
          `FOR r IN ${collection}
REMOVE r IN ${collection}`,
        )).collect();
      }
    });

    await t.step("Story Stuff", async (t) => {
      await t.step("Add story1", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addStory(story1);
        test.assertEquals(added.name, story1.name);
        test.assertEquals(added.lastScrape, story1.lastScrape);
        storyID = added._id;
      });
      await t.step("Add story2", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addStory(story2);
        test.assertEquals(added.name, story2.name);
        const last = await store.storyById(story1.domain, story1.id);
        test.assertEquals(last?.name, story1.name);
        test.assertNotEquals(added._id, storyID);
      });
      await t.step("Update story1 as story3", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addStory(story3);
        test.assertEquals(added.dateupdate, story3.dateupdate);
        test.assertArrayIncludes(added.tags, [story3.tags[1]]);
        test.assertEquals(added._id, storyID);
        test.assertEquals(added.lastScrape, story3.lastScrape);
        test.assertEquals(added.leadcount, story3.leadcount);
      });
      await t.step("byId story2 and delete", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.storyById(story2.domain, story2.id);
        test.assertStrictEquals(result?.name, story2.name);
        await result.delete();
      });
      await t.step("byId deleted", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.storyById(story2.domain, story2.id);
        test.assertStrictEquals(result, null);
      });
    });
    await t.step("User Stuff", async (t) => {
      await t.step("Add user1", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addUser(user1);
        test.assertEquals(added.name, user1.name);
        test.assertEquals(added.lastScrape, user1.lastScrape);
        userID = added._id;
      });
      await t.step("Add user2", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addUser(user2);
        test.assertEquals(added.name, user2.name);
      });
      await t.step("Update user1 as user3", async () => {
        if (!store) {
          test.unreachable();
        }
        const added = await store.addUser(user3);
        test.assertEquals(added.name, user3.name);
        test.assertEquals(added._id, userID);
        test.assertEquals(added.lastScrape, user3.lastScrape);
      });
      await t.step("byId user2", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.userById(user2.domain, user2.id);
        test.equal(result?.name, user2.name);
      });
      await t.step("byId user2 and delete", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.userByName(user2.domain, user2.name);
        test.equal(result?.id, user2.id);
        await result?.delete();
      });
      await t.step("byId deleted", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.userById(user2.domain, user2.id);
        test.assertStrictEquals(result, null);
      });
      await t.step("byName deleted", async () => {
        if (!store) {
          test.unreachable();
        }
        const result = await store.userByName(user2.domain, user2.name);
        test.assertStrictEquals(result, null);
      });
    });
    await t.step("Verify IDs", () => {
      if (!store || !storyID || !userID) {
        test.unreachable();
      }
      RecEngineStore.areIDs(userID, storyID);
    });
    await t.step("Review Stuff", async (t) => {
      await t.step("add Review1", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const reviewGroup: ReviewGroup = {};
        reviewGroup[review1.time + ""] = review1;
        const pointer: ReviewPointer = {
          _to: userID,
          _from: storyID,
          reviews: reviewGroup,
        };
        await store.addReview(pointer);
      });
      await t.step("add Review2 to Review 1", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const reviewGroup: ReviewGroup = {};
        reviewGroup[review2.time + ""] = review2;
        const pointer: ReviewPointer = {
          _to: userID,
          _from: storyID,
          reviews: reviewGroup,
        };
        const added = await store.addReview(pointer);
        test.assertEquals(
          added.reviews[review1.time + ""].review,
          review1.review,
        );
        test.assertEquals(added.reviews[review1.time + ""].time, review1.time);
        test.assertEquals(
          added.reviews[review2.time + ""].review,
          review2.review,
        );
        test.assertEquals(added.reviews[review2.time + ""].time, review2.time);
      });
      await t.step("storyReviews", async () => {
        if (!store || !storyID) {
          test.unreachable();
        }
        const result = await store.storyReviews(storyID);
        test.assert(result.length > 0, "Review didn't get added?!");
        let res1;
        if (result && result[0]) {
          res1 = result[0];
        }
        if (!res1) {
          test.unreachable();
        }
        test.assertStringIncludes(res1._from, storyID);
        userID = res1._to;
      });
    });
    await t.step("Fave Stuff", async (t) => {
      let recKey: string | null = null;
      await t.step("add fave", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const added = await store.addFave({
          _to: storyID,
          _from: userID,
        });
        recKey = added._key;
      });
      await t.step("add fave", async () => {
        if (!store || !storyID || !userID || !recKey) {
          test.unreachable();
        }
        await store.addFave({
          _to: storyID,
          _from: userID,
        });
        const added = await store.addFave({
          _to: storyID,
          _from: userID,
        });
        test.assertEquals(added._key, recKey);
      });
      await t.step("byUser", async () => {
        if (!store || !userID) {
          test.unreachable();
        }
        const result = await store.userFaves(userID);
        test.assertNotStrictEquals(result, null);
        test.assert(result.length > 0, "Fave didn't get added?!");
        let res1;
        if (result && result[0]) {
          res1 = result[0];
        }
        if (!res1) {
          test.unreachable();
        }
        const user = (await res1.from());
        test.equal(user.id, user3.id);
        test.equal(user.name, user3.name);
        const story = (await res1.to());
        test.equal(story.name, story3.name);
      });
      await t.step("byStory", async () => {
        if (!store || !storyID) {
          test.unreachable();
        }
        const result = await store.workFaved(storyID);
        test.assertNotStrictEquals(result, null);
        test.assert(result.length > 0, "Fave didn't get added?!");
        let res1;
        if (result && result[0]) {
          res1 = result[0];
        }
        if (!res1) {
          test.unreachable();
        }
        const user = (await res1.from());
        test.equal(user.id, user3.id);
        test.equal(user.name, user3.name);
        const story = (await res1.to());
        test.equal(story.name, story3.name);
      });
    });
    await t.step("Author Stuff", async (t) => {
      let recKey: string | null = null;
      await t.step("add author", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const added = await store.addAuthor({
          _to: storyID,
          _from: userID,
        });
        recKey = added._key;
      });
      await t.step("try again", async () => {
        if (!store || !storyID || !userID || !recKey) {
          test.unreachable();
        }
        const added = await store.addAuthor({
          _to: storyID,
          _from: userID,
        });
        test.assertEquals(added._key, recKey);
      });
      await t.step("byStory", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const result = await store.workAuthors(storyID);
        test.assertNotStrictEquals(result, null);
        test.assert(result.length > 0, "Author didn't get added?!");
        let res1;
        if (result && result[0]) {
          res1 = result[0];
        }
        if (!res1) {
          test.unreachable();
        }
        const user = (await res1.from());
        test.equal(user.id, user3.id);
        test.equal(user.name, user3.name);
        const story = (await res1.to());
        test.equal(story.name, story3.name);
      });
      await t.step("byUser", async () => {
        if (!store || !storyID || !userID) {
          test.unreachable();
        }
        const result = await store.authorsWork(userID);
        test.assertNotStrictEquals(result, null);
        test.assert(result.length > 0, "Author didn't get added?!");
        let res1;
        if (result && result[0]) {
          res1 = result[0];
        }
        if (!res1) {
          test.unreachable();
        }
        const user = (await res1.from());
        test.equal(user.id, user3.id);
        test.equal(user.name, user3.name);
        const story = (await res1.to());
        test.equal(story.name, story3.name);
      });
    });
    await t.step("Dump Stuff", async (t) => {
      await t.step("User", async () => {
        if (!store) {
          test.unreachable();
        }
        const res = await store.dumpUser(user3.domain, user3.id);
        test.assertNotEquals(res, null);
        if (!res) {
          test.unreachable();
        }
        test.assertEquals(res.domain, user3.domain);
        test.assertEquals(res.id, user3.id);
        test.assertEquals(res.name, user3.name);
        test.assertEquals(res.lastScrape, user3.lastScrape);
        test.assert(res.authored.length > 0, "Missing Authored");
        test.assert(res.favedBooks.length > 0, "Missing Fave");
        test.assertEquals(res.authored[0].domain, story3.domain);
        test.assertEquals(res.authored[0].id, story3.id);
        test.assertEquals(res.authored[0].name, story3.name);
        test.assertEquals(res.favedBooks[0].domain, story3.domain);
        test.assertEquals(res.favedBooks[0].id, story3.id);
        test.assertEquals(res.favedBooks[0].name, story3.name);
      });
      await t.step("Story", async () => {
        if (!store) {
          test.unreachable();
        }
        const res = await store.dumpStory(story3.domain, story3.id);
        test.assertNotEquals(res, null);
        if (!res) {
          test.unreachable();
        }
        test.assertEquals(res.domain, story3.domain);
        test.assertEquals(res.id, story3.id);
        test.assertEquals(res.details.id, story3.id);
        test.assertEquals(res.details.lastScrape, story3.lastScrape);
        test.assertEquals(res.details.name, story3.name);
        test.assertEquals(res.details.blurb, story3.blurb);
        test.assertEquals(res.details.tags, story3.tags);
        test.assertEquals(res.details.fandoms, story3.fandoms);
        test.assertEquals(res.details.domain, story3.domain);
        test.assertEquals(res.details.dateupdate, story3.dateupdate);
        test.assert(res.authors.length > 0, "Missing Authors");
        test.assert(res.faved.length > 0, "Missing Fave");
        test.assertEquals(res.authors[0].domain, user3.domain);
        test.assertEquals(res.authors[0].id, user3.id);
        test.assertEquals(res.authors[0].name, user3.name);
        test.assertEquals(res.faved[0].domain, user3.domain);
        test.assertEquals(res.faved[0].id, user3.id);
        test.assertEquals(res.faved[0].name, user3.name);
      });
      await t.step("Reviews", async () => {
        if (!store) {
          test.unreachable();
        }
        const res = await store.dumpLeads(story3.domain, story3.id);
        test.assertNotEquals(res, null);
        if (!res) {
          test.unreachable();
        }
        test.assertEquals(res.domain, story3.domain);
        test.assertEquals(res.id, story3.id);
        test.assertEquals(res.leadcount, story3.leadcount);
        test.assert(res.reviewed.length == 1, "Missing Review");
        test.assertEquals(res.reviewed[0].domain, user3.domain);
        test.assertEquals(res.reviewed[0].id, user3.id);
      });

      await t.step("Nulls with bad ID's", async () => {
        if (!store) {
          test.unreachable();
        }
        {
          const res = await store.dumpLeads("ffnet", 555);
          test.assertEquals(res, null);
        }
        {
          const res = await store.dumpStory("ffnet", 555);
          test.assertEquals(res, null);
        }
        {
          const res = await store.dumpUser("ffnet", 555);
          test.assertEquals(res, null);
        }
      });
    });
  },
});
